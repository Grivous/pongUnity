﻿Shader "Holistic/shaderWall" {
	Properties {
		_myColor ("Color", Color) = (1,1,1,1)
		_myEmmission ("Emission", Color) = (1,1,1,1)
	}
	SubShader {

		CGPROGRAM
			#pragma surface surf Lambert

			struct Input {
				float2 uvMainTex;
			};

			fixed4 _myColor;
			fixed4 _myEmmission;

			void surf (Input IN, inout SurfaceOutput o) {
				o.Albedo = _myColor.rgb;
				o.Emission = _myEmmission.rgb;
			}
		ENDCG
	}
	FallBack "Diffuse"
}

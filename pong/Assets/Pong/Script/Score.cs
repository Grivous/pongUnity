﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Score : NetworkBehaviour {

    [SyncVar]  public int score1 = 0;
    [SyncVar] public int score2 = 0;
    public GameObject ball;
    public Text score;

    public void Start()
    {
        score = GameObject.Find("MainScore").GetComponent<Text>();
    }
 
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            CmdResetScore();
        }
    }

    void OnTriggerEnter(Collider target)
    {
        if (target.gameObject.tag == "Goal1")
        {
            CmdServerAjoutPoint1();
        }
        else if (target.gameObject.tag == "Goal2")
        {
            CmdServerAjoutPoint2();
        }
        
    }

    [Command]
    void CmdResetScore()
    {
        score1 = 0;
        score2 = 0;
        RpcMajScore(score1, score2);
    }

    [Command]
    void CmdServerAjoutPoint1()
    {
        score1++;
        RpcMajScore(score1, score2);
    }

    [Command]
    void CmdServerAjoutPoint2()
    {
        score2++;
        RpcMajScore(score1, score2);

    }

    [ClientRpc]
    public void RpcMajScore(int score1, int score2)
    {
        GameObject.Find("MainScore").GetComponent<Text>().text = score2 + " - " + score1;
    }

}

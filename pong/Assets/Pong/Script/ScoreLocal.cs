﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreLocal : MonoBehaviour {

    public int score1 = 0;
    public int score2 = 0;
    private Text score;

    public void Start()
    {
        score = GameObject.Find("MainScore").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            resetScore();
        }
    }

    void OnTriggerEnter(Collider target)
    {
        if (target.gameObject.tag == "Goal1")
        {
            AjoutPoint1();
        }
        else if (target.gameObject.tag == "Goal2")
        {
            AjoutPoint2();
        }
        
    }  
    
    void AjoutPoint1()
    {
        score1++;
        MajScore(score1, score2);
    }
    void AjoutPoint2()
    {
        score2++;
        MajScore(score1, score2);
    }

    void resetScore()
    {
        score1 = 0;
        score2 = 0;
        MajScore(score1, score2);
    }

    public void MajScore(int score1, int score2)
    {
        GameObject.Find("MainScore").GetComponent<Text>().text = score2 + " - " + score1;
        if (score1 == 10)
        {
            GameObject.Find("MainScore").GetComponent<Text>().text = "DEFAITE - VICTOIRE";
        }
        else if (score2 == 10)
        {
            GameObject.Find("MainScore").GetComponent<Text>().text = "VICTOIRE - DEFAITE";
        }
    }

}

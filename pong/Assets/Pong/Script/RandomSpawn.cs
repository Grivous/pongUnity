﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RandomSpawn : NetworkBehaviour {

    private float sx;
    private float sy;

    // Use this for initialization
    void Start () {
        if (!isServer)
        {
            return;
        }
        sx = Random.Range(-9, 9);
        sy = Random.Range(-5, 5);
        transform.position = new Vector3(sx, sy, 0);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

}

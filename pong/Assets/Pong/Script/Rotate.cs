﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {
    public float speed;
    public Vector3 vectorRotate = new Vector3(1, 1, 1);
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(vectorRotate, (speed * 50) * Time.deltaTime, 0);
    }
}

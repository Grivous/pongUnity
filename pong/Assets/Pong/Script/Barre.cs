﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class Barre : NetworkBehaviour {

    public GameObject ballPrefab;

    public float speed; 
    public int top = 4;
    public ParticleSystem ps;
    public Vector3 mousePosition;
    public Vector3 startPosition;
    private int nbBall;
    

    // Use this for initialization
    void Start () {
        ps.Play();
        startPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {

        if (!isLocalPlayer)
            return;




        //transform.Translate(0f, Input.GetAxis("Vertical2") * speed * Time.deltaTime, 0f);

            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mousePosition.x, mousePosition.y, 0);

        if (startPosition.x < 0 && transform.position.x >= 0)//barre de gauche
        {
            transform.position = new Vector3(0 , transform.position.y , transform.position.z);
        }
        else if (startPosition.x > 0 && transform.position.x <= 0)//barre de droite
        {
            transform.position = new Vector3(0, transform.position.y, transform.position.z);
        }

        if (transform.position.y > 5.5)//ne pas passer a travers en haut
        {
            transform.position = new Vector3(transform.position.x, 5.5f, transform.position.z);
        }
        else if (transform.position.y < -5.5)//ne pas passer a travers en bas
        {
            transform.position = new Vector3(transform.position.x, -5.5f, transform.position.z);
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            // Command function is called from the client, but invoked on the server
            CmdFire();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Command function is called from the client, but invoked on the server
            SceneManager.LoadScene(1);
        }

    }



    [Command]
    void CmdFire()
    {
        if (!isServer)
        {
            return;
        }
        if (GameObject.FindGameObjectsWithTag("Ball").Length >= 1)
        {
            GameObject.FindGameObjectWithTag("Ball").transform.position = new Vector3(0, 0, 0);
        }
        else
        {
        var ball = (GameObject)Instantiate(ballPrefab);
        NetworkServer.Spawn(ball);
        }
    }


}

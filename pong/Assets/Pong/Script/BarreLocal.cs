﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class BarreLocal : MonoBehaviour {

    public GameObject ballPrefab;

    public bool isPlayer1;


    public float speed; 
    public int top = 4;
    public Vector3 mousePosition;
    public Vector3 startPosition;
    private int nbBall;
    

    // Use this for initialization
    void Start () {
        startPosition = transform.position;
    }



    // Update is called once per frame
    void Update()
    {
        if (isPlayer1)
        {
            if (Input.GetAxis("Vertical") != 0)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + Input.GetAxis("Vertical") * Time.deltaTime * 10, 0f);
            }
            if (Input.GetKey("q"))
            {
                transform.Rotate(0f, 0f, speed * 10 * Time.deltaTime);
            }
            else if (Input.GetKey("d"))
            {
                transform.Rotate(0f, 0f, -speed * 10 * Time.deltaTime);
            }
            else if (Input.GetKeyUp("q") | Input.GetKeyUp("d"))
            {
                transform.Rotate(0f, 0f, 0f);
            }
            if (Input.GetKey("h"))
            {
                Fire();
            }

        }
        if (!isPlayer1)
        {
            if (Input.GetAxis("Vertical2") != 0)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + Input.GetAxis("Vertical2") * Time.deltaTime * 10, 0f);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Rotate(0f, 0f, speed * 10 * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.Rotate(0f, 0f, -speed * 10 * Time.deltaTime);
            }
            if (Input.GetKeyUp(KeyCode.RightArrow) | Input.GetKeyUp(KeyCode.LeftArrow  ))
            {
                transform.Rotate(0f, 0f, 0f);
            }
        }
        if (transform.position.y > 5.5)//ne pas passer a travers en haut
        {
            transform.position = new Vector3(transform.position.x, 5.5f, transform.position.z);
        }
        else if (transform.position.y < -5.5)//ne pas passer a travers en bas
        {
            transform.position = new Vector3(transform.position.x, -5.5f, transform.position.z);
        }


    }


    void Fire()
    {

        if (GameObject.FindGameObjectsWithTag("Ball").Length >= 1)
        {
            GameObject.FindGameObjectWithTag("Ball").transform.position = new Vector3(0, 0, 0);
        }
        else
        {
        var ball = (GameObject)Instantiate(ballPrefab);
        }
    }


}

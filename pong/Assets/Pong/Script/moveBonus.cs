﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBonus : MonoBehaviour {

    
    private bool etat;//1 = droite haut  0 = gauche bas
    public bool hautBas = false;//1haut bas       0 gauche droite

    // Use this for initialization
    void Start () {
        etat = true;
    }
	
	// Update is called once per frame
	void Update () {


        if (hautBas)//haut bas
        {
            if (transform.position.y >= 5.5 || transform.position.y <= -5.5)
                etat = !etat;
            
            if (etat)//haut
            {
                transform.Translate(0f, 0.05f, 0f);

            }
            else//bas
            {
                transform.Translate(0f, -0.05f, 0f);
            }

        }
        else//gauche droite
        {
            if (transform.position.x >= 7 || transform.position.x <= -7)
                etat = !etat;
            if (etat)//droite
            {
                transform.Translate(0.05f, 0f, 0f);
            }
            else//gauche
            {
                transform.Translate(-0.05f, 0f, 0f);
            }

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shaderColor : MonoBehaviour
{
    private bool color = false;
    private Renderer rend;
    // Use this for initialization
    void Start()
    {
        rend = GetComponent<Renderer>();
        //rend.material.shader = Shader.Find("Holistic/shaderWall");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider target)
    {
        if (target.gameObject.tag == "Ball")
        {
            color = !color;
            if (color)
            {
                rend.material.SetColor("_myColor", Color.red);
                rend.material.SetColor("_myEmission", Color.red);
            }
            else
            {
                rend.material.SetColor("_myColor", Color.blue);
                rend.material.SetColor("_myEmission", Color.blue);
            }
        }
    }
}

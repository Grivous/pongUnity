﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Ball : NetworkBehaviour {

    public float speed;
    public Rigidbody rb;
    public Text score;
    private float sx;
    private float sy;
    private Vector3 lastPosition;
    private Vector3 direction;

    public ParticleSystem ps;
    public AudioSource soundElec;
    
    


    // Use this for initialization
    void Start () {
        sx = Random.Range(0, 2) == 0 ? -1 : 1;
        sy = Random.Range(0, 2) == 0 ? -1 : 1;
        rb = GetComponent<Rigidbody>();
        speed = 5f;
        rb.transform.position = new Vector3(0, 0, 0);
        rb.velocity = new Vector3(speed * sx, speed * sy, 0f);



    }
    void resetBall()
    {
        sx = Random.Range(0, 2) == 0 ? -1 : 1;
        sy = Random.Range(0, 2) == 0 ? -1 : 1;
        rb = GetComponent<Rigidbody>();
        speed = 5f;
        rb.transform.position = new Vector3(1, 1, 0);
        rb.velocity = new Vector3(speed * sx, speed * sy, 0f);

    }
    
    void Update () {

        direction = transform.position - lastPosition;
        lastPosition = transform.position;

        ps.transform.position = transform.position;
    }

    void OnTriggerEnter(Collider target)
    {
        if (target.gameObject.tag == "Goal1" || target.gameObject.tag == "Goal2")
            resetBall();

        if (target.gameObject.tag == "onBarre")
        {
            ps.Play();
            soundElec.Play();

        }

        if (target.gameObject.tag == "Speed")
        {
            rb.AddForce(direction * 1000f);
        }

    }


}


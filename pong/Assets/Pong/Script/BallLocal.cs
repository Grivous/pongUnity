﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class BallLocal : MonoBehaviour {

    public float speed = 5f;
    public Rigidbody rb;
    private float sx;
    private float sy;
    private Vector3 lastPosition;
    private Vector3 direction;
    
    
    


    // Use this for initialization
    void Start () {
        sx = Random.Range(0, 2) == 0 ? -1 : 1;
        sy = Random.Range(0, 2) == 0 ? -1 : 1;
        rb = GetComponent<Rigidbody>();
        speed = 5f;
        rb.transform.position = new Vector3(0, 0, 0);
        rb.velocity = new Vector3(speed * sx, speed * sy, 0f);



    }
    void resetBall()
    {
        sx = Random.Range(0, 2) == 0 ? -1 : 1;
        sy = Random.Range(0, 2) == 0 ? -1 : 1;
        rb = GetComponent<Rigidbody>();
        speed = 5f;
        rb.transform.position = new Vector3(0, 0, 0);
        rb.velocity = new Vector3(speed * sx, speed * sy, 0f);  
    }
    
    void Update () {

        direction = transform.position - lastPosition;
        lastPosition = transform.position;
        
    }

    void OnTriggerEnter(Collider target)
    {
        if (target.gameObject.tag == "Goal1" || target.gameObject.tag == "Goal2")
            resetBall();
        

    }


}

